package mx.tecnm.misantla.appgeneraciones

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {

            val anio = edtAnio.text.toString().toInt()

            when(anio){
                in 1930..1948 ->{
                   tvResultado.text = "SILENT GENERATION"
                    imgGeneration.setImageResource(R.drawable.gsg)

                }
                in 1949..1968 ->{
                    tvResultado.text =   "BABY BOOM"
                    imgGeneration.setImageResource(R.drawable.gbb)
                }
                in 1969..1980 -> {
                    tvResultado.text =  "GENERATION X"
                    imgGeneration.setImageResource(R.drawable.gx)
                }
                in 1981..1993 ->{
                    tvResultado.text =  "GENERATION Y"
                    imgGeneration.setImageResource(R.drawable.gy)
                }
                in 1994..2010 ->{
                    tvResultado.text =  "GENERATION Z"
                    imgGeneration.setImageResource(R.drawable.generationz)
                }
                else ->{
                    tvResultado.text =    "NO DEFINIDA"
                }
            }
           // tvResultado.setText(generacion)

       /*    if( (anio >=1930) && (anio <= 1948)){
               tvResultado.text="Silent Generation"
           }
            if( (anio >=1949) && (anio <= 1968)){
                tvResultado.text="Baby Boom"
            }
            if( (anio >=1969) && (anio <= 1980)){
                tvResultado.text="Generation X"
            }
            if( (anio >=1981) && (anio <= 1993)){
                tvResultado.text="Generation Y"
            }
            if( (anio >=1994) && (anio <= 2010)){
                tvResultado.text="Generation Z"
            }
*/
        }
    }
}